#include "SelectionSort.h"
#include <iostream>
using namespace std;

SelectionSorting::SelectionSorting()
{

}
SelectionSorting::~SelectionSorting()
{

}
void inline Swap(int &firstVlaue, int &secondValue)
{
	int temp = firstVlaue;
	firstVlaue = secondValue;
	secondValue = temp;
}
void SelectionSorting::SelectionSorts(int array[], int array_size)
{
	cout << "SelectionSort: " << endl;
	for (int holdingValueOne = 0; holdingValueOne < array_size - 1; ++holdingValueOne)
	{
		int minimum = holdingValueOne;
		for (int holdingValueTwo = holdingValueOne + 1; holdingValueTwo < array_size; ++holdingValueTwo)
		{
			if (array[holdingValueTwo] < array[minimum])
				minimum = holdingValueTwo;
		}
		
        Swap(array[minimum], array[holdingValueOne]);
		for (int temp = 0; temp < array_size; ++temp)
		{
			cout << array[temp] << " ";
		}
		cout << endl;
	}
}
