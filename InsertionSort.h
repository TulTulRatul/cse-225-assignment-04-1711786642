#ifndef INSERTION_H_INCLUDED
#define INSERTION_H_INCLUDED
#include <vector>

using namespace std;

class InsertionSorting
{
public:
	InsertionSorting();
	~InsertionSorting();
	void InsertionSorts(int arr[], int arr_size);
	void InsertionSorts(vector<int>, int size);
};
#endif // !INSERTION_H_INCLUDED
