#include "BubbleSort.h"

#include "InsertionSort.h"

#include "MergeSort.h"

#include "SelectionSort.h"

#include <vector>

#include <iostream>

using namespace std;


int main()
{
	BubbleSort Bubble_Sorting;
	
    int objectFile[8] = { 14,33,27,10,35,19,48,44 };
	
    int a = sizeof(objectFile) / sizeof(objectFile[0]);
	Bubble_Sorting.BubbleSorting(objectFile, a);

	SelectionSorting Selection_Sorting;
	
    int objectFile1[8] = { 14,33,27,10,35,19,48,44 };
	
    int b = sizeof(objectFile1) / sizeof(objectFile1[0]);
	Selection_Sorting.SelectionSorts(objectFile1, b);

	InsertionSorting Insertion_Sorting;
	
    int objectFile2[8] = { 14,33,27,10,35,19,48,44 };
	
    int m = sizeof(objectFile2) / sizeof(objectFile2[0]);
	Insertion_Sorting.InsertionSorts(objectFile2, m);
	
	MergeSorting Merge_Sorting;
	
    int objectFile3[8] = { 14,33,27,10,35,19,48,44 };
	
    int n = sizeof(objectFile3) / sizeof(objectFile3[0]);
	Merge_Sorting.MergeSorts(objectFile3, n);
}
